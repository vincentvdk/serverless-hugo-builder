#!/usr/bin/env python
# coding=utf-8
import subprocess
import tarfile
import json
import logging
import os
import stat
from mimetypes import MimeTypes
import requests
import boto3

from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

OUTPUT_BUCKET = os.environ['OUTPUT_BUCKET']
GL_PRIVATE_TOKEN = os.environ['GL_PRIVATE_TOKEN']
GL_PROJECT_ID = os.environ['GL_PROJECT_ID']
HUGO_BASE_URI = os.environ['HUGO_BASE_URI']


def _generate_static_site(archive_url, source_dir, site_dir):
    # Run Hugo static site generator
    # Download the Hugo blog source archive from GitLab
    logger.info('Download hugo website source from %s' % archive_url)
    try:
        with open('/tmp/archive.tar.gz', "wb") as file:
            archive = requests.get(archive_url)
            file.write(archive.content)
    except Exception as e:
        logger.error(e)

    # Download hugo binary
    logger.info('Donwloading Hugo')
    try:
        hugo = requests.get(
            'https://github.com/gohugoio/hugo/releases/download/v0.55.6/hugo_0.55.6_Linux-64bit.tar.gz',
            stream=True)
        with open('/tmp/hugo.tar.gz', 'wb') as hugo_file:
            for chunk in hugo.iter_content(chunk_size=1024):
                if chunk:
                    hugo_file.write(chunk)
    except Exception as e:
        raise

    # Unpack files
    logger.info('Unpacking Hugo')
    try:
        hugo_tar = tarfile.open('/tmp/hugo.tar.gz')
        hugo_tar.extractall(path='/tmp')
        hugo_tar.close()
        #os.chmod('/tmp/hugo', stat.S_IEXEC)
    except Exception as e:
        logger.error("Failed unpacking Hugo %s", e)

    logger.info('Unpack website source')
    try:
        archive_tar = tarfile.open('/tmp/archive.tar.gz')
        archive_tar.extractall(path='/tmp')
        archive_tar.close()
        logger.info("unpack archive done")
    except Exception as e:
        logger.error("Failed unpacking website source archive. Error: %s", e)

    # build the website
    logger.info("Start building Hugo website")
    try:
        print(source_dir)
        print(site_dir)
        command = ["/tmp/hugo", "-v", "--source=" + source_dir, "--destination=" + site_dir, "-b" + HUGO_BASE_URI]
        # logger.info(subprocess.check_output(command, stderr=subprocess.STDOUT))
        subprocess.run(command)
    except subprocess.CalledProcessError as e:
        logger.error("ERROR return code: ", e.returncode)
        logger.error("ERROR output: ", e.output)
        raise


def _upload_blog(source_dir, bucket_name):
    try:
        s3_client = boto3.client('s3', region_name='eu-west-1')
        for root, dirs, files in os.walk(source_dir):
            for filename in files:
                # construct path
                local_path = os.path.join(root, filename)
                # detect filetype
                ftype, encoding = MimeTypes().guess_type(local_path)
                # Construct relative path
                relative_path = os.path.relpath(local_path, '/tmp/dest')
                # start upload
                logger.info('Uploading %s to Amazon S3 bucket %s' %
                            (filename, bucket_name))
                s3_client.upload_file(local_path, bucket_name, relative_path,
                                      ExtraArgs={'ContentType': ftype})
                logger.info('File uploaded to https://s3.%s.amazonaws.com/%s/%s' % (
                    'eu-west-1', bucket_name, filename))
    except ClientError as e:
        logger.error(e)
    except Exception as e:
        logger.error('Upload to s3 failed %s', e)


def lambda_handler(event, context):
    body = json.loads(event['Records'][0]['Sns']['Message'])
    hugo_source_path = body['checkout_sha']

    web_url = 'https://gitlab.com/api/v4/projects/' + GL_PROJECT_ID + '/repository/archive?private_token=' + GL_PRIVATE_TOKEN
    try:
        # Generate static site
        # name after /tmp should be variable
        _generate_static_site(web_url, '/tmp/vanderkussen.org-master-' + hugo_source_path, '/tmp/dest')
        # upload output to s3
        _upload_blog('/tmp/dest', OUTPUT_BUCKET)
    except Exception as e:
        logger.error('Building site failed. Error: %s', e)
