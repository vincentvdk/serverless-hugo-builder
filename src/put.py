#!/usr/bin/env python
# coding=utf-8
import os
import json
import boto3
import logging
from botocore.exceptions import ClientError

GITLAB_TOKEN = os.environ['GitlabToken']
TOPIC_ARN = os.environ['SNS_TOPIC']

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def _check_gitlab_token(token):
    """Check if token matches and raise exception if not"""
    if GITLAB_TOKEN == token:
        pass
    else:
        raise


def _response(message, status_code):
    return {
        'statusCode': str(status_code),
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
            },
        }


def _construct_sns(message):
    client = boto3.client('sns')
    client.publish(
        TopicArn=TOPIC_ARN,
        Message=message,
        MessageStructure='json',
    )


def lambda_handler(event, context):
    try:
        msg = {"default": event['body']}
        _check_gitlab_token(event['headers']['X-Gitlab-Token'])
        _construct_sns(json.dumps(msg))
        return _response('Gitlab token OK', 200)
    except ClientError as e:
        logger.error(e)
    except Exception as e:
        logger.error(e)
        return _response('Gitlab token wrong or not set', 401)
