.PHONY: zip
.PHONY: clean
.PHONY: upload-lambda

TAG="\n\n\033[0;32m\#\#\# "
END=" \#\#\# \033[0m\n"

# Read configuration
include config.cfg

# Functions
check_defined = \
		$(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

clean:
	@echo -e ${TAG}Cleaning up${END}
	rm -rf .tox *.egg dist build .coverage
	find . -name '__pycache__' -delete -print -o -name '*.pyc' -delete -print
	rm -f ${FUNCTION_NAME}.zip
	@echo

zip: clean
	@echo -e ${TAG}Create Lambda zip artifact${END}
	# Add non stdlib modules.
	pip install -r requirements.txt
	cd src; zip -r ../${FUNCTION_NAME}.zip * -i put.py generate.py
	cd ${VIRTUAL_ENV}/lib/python3.6/site-packages; zip -r $(abspath $(dir $(lastword $(MAKEFILE_LIST))))/${FUNCTION_NAME}.zip *

upload: zip
	@echo -e ${TAG}Upload Lambda zip artifact${END}
	aws s3 cp ${FUNCTION_NAME}.zip s3://${LAMBDA_SRC_BUCKET}/

deploy: upload
	@echo -e ${TAG}Creating Cloudformation Stack ${STACKNAME} ${END}
	@aws cloudformation create-stack --stack-name ${STACKNAME} --template-body file://cfn-template.yaml --capabilities CAPABILITY_NAMED_IAM --output table --parameters \
		ParameterKey=LambdaSourceBucketParam,ParameterValue=${LAMBDA_SRC_BUCKET} \
		ParameterKey=LambdaSourceBucketKeyParam,ParameterValue=${FUNCTION_NAME}.zip \
		ParameterKey=URLFQDN,ParameterValue=${URLFQDN} \
		ParameterKey=LambdaEdgeFunctionVersion,ParameterValue=${LAMBDA_EDGE_FUNCTION_VERSION} \
		ParameterKey=CertArn,ParameterValue=${CERTARN} \
		ParameterKey=GitlabPrivateToken,ParameterValue=${GITLAB_PRIVATE_TOKEN} \
		ParameterKey=GitlabProjectId,ParameterValue=${GITLAB_PROJECT_ID} \
		ParameterKey=HugoBaseURI,ParameterValue=${HUGO_BASE_URI}
	@echo -e ${TAG}Waiting for Cloudformation stack to complete...${END}
	@aws cloudformation wait stack-create-complete --stack-name ${STACKNAME}
	@echo -e ${TAG}Deployment complete!${END}

delete:
	@echo -e ${TAG}Deleting Cloudformation Stack ${STACKNAME} ${END} 
	@aws cloudformation delete-stack --stack-name ${STACKNAME}

update: zip upload
	@echo -e ${TAG}Update Lambda Function ${END}
	@aws lambda update-function-code --s3-bucket ${LAMBDA_SRC_BUCKET} --s3-key ${FUNCTION_NAME}.zip --function-name ${FUNCTION_NAME}

update-stack:
	@echo -e ${TAG}Updating Cloudformation Stack ${STACKNAME} ${END}
	@aws cloudformation update-stack --stack-name ${STACKNAME} --template-body file://cfn-template.yaml --capabilities CAPABILITY_NAMED_IAM --output table --parameters \
		ParameterKey=LambdaSourceBucketParam,UsePreviousValue=true \
		ParameterKey=LambdaSourceBucketKeyParam,UsePreviousValue=true \
		ParameterKey=URLFQDN,UsePreviousValue=true \
		ParameterKey=LambdaEdgeFunctionVersion,ParameterValue=${LAMBDA_EDGE_FUNCTION_VERSION} \
		ParameterKey=CertArn,UsePreviousValue=true \
		ParameterKey=GitlabPrivateToken,UsePreviousValue=true \
		ParameterKey=GitlabProjectId,ParameterValue=${GITLAB_PROJECT_ID} \
		ParameterKey=HugoBaseURI,ParameterValue=${HUGO_BASE_URI}
	@echo -e ${TAG}Waiting for Cloudformation stack update to complete...${END}
	@aws cloudformation wait stack-update-complete --stack-name ${STACKNAME}
	@echo -e ${TAG}Update complete!${END}


# Run clean-pyc, lint and coverage targets.
fulltest: clean-pyc lint coverage
.PHONY: fulltest

help:
	@echo "clean:           general cleanup"
	@echo "zip:             create a new Lambda deployment package"
	@echo "upload-lambda:   deploy the lambda package to the dpp-iac bucket"
	@echo "deploy:          deploy the Squad CFN stack"
	@echo "update:          update the Lambda Function"
	@echo "delete:          delete the Squad CFN stack"
