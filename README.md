# Serverless Hugo Builder
This project's goal is to automate the build of a website using the Hugo
static site generator and serve it using AWS services.

All resources are deployed using a Cloudformation template (cfn-template.yaml).

## Prerequisites
### ACM (Certificate Manager)
To enable TLS on the Cloudfront distribution a certificate in **us-east-1**
(region is important here) is needed for the domain you want to use. The
certificate's **ARN** is needed to deploy the Cloudformation stack

### S3 bucket
An S3 bucket to uplaod the Lambda function as a package is needed. This needs to
be created before deploying the Cloudformation stack.

## Deploy the stack
In the repo you can find a `Makefile` which makes it easier to create the Lambda
package and upload it to an S3 bucket

### Configuration
The `Makefile` reads configuration from a file with the name `config.cfg`. An
example (config.cfg.example) is included in the repo. You can use it to see what
configuration options are needed or you can rename it and update it.

### Build a Lambda package
Because we use some extra Python modules we need to create a 'Lambda Packge'.

First create a Python virtualenv by running following from within the repo
```
$ virtualenv -p python3 venv
```

Enable the virtualenv
```
$ source venv/bin/activate
```


To build and upload the Lambda package run the following command:
```
$ make upload
```


## Post install
### DNS
The website will be served using Cloudfront. To use your own domain you will
need to create a CNAME record for the fqdn you want to use and point it to the
cloudfront domain name. 

example:

```
$ dig +noall +answer -t cname blog.vanderkussen.org
blog.vanderkussen.org.  3337    IN      CNAME   d2rvip4q9r0icc.cloudfront.net.
```

## Gitlab configuration
### Generate API token
Account -> Access Tokens -> Personal Access Tokens


